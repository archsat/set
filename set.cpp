#include <iostream>

template <typename T>
class set {
private:
	int Size;
	template <typename U>
	class set_unit {
	public:
		set_unit(U content = U(), set_unit* next = nullptr) {
			this->content = content;
			this->next = next;
		}
		~set_unit() {
			this->content = U();
		}
		T content;
		set_unit* next;
		//Перегрузка операторов сравнения для элементов множества.
		bool operator==(const set_unit<U>& u) {
			return (this->content == u.content);
		};
		bool operator!=(const set_unit<U>& u) { return !(this == u); };
	};
	//Функция удаляет первый добавленный элемент из множества.
	void removeFirst();
public:
	set(); //Конструктор по умолчанию - создает пустое множество.
	set(set<T>&); //Конструктор копирования
	template<typename... Args> 
	set(Args...); //Конструктор создания непустого множества
	~set(); //Деструктор по умолчанию
	set_unit<T>* first; //Указатель на первый элемент множества.
	int getSize() { return Size; }; //Возвращает мощность множества.
	void remove(T); //Удаляет строго один элемент.
	template<typename... Args>
	void remove(T, Args...); //Удаляет несколько элементов.
	void remove(set&); //Удаляет подмножество.
	void clear(); //Удаляет все элементы множества
	bool isExists(T); //Проверяет, если ли в множестве элемент
	template<typename ...Args>
	void add(T); //Функция add для одного аргумента.
	template<typename... Args>
	void add(T, Args...); //Перегруженная функция add - рекурсивная функция, добавляет произвольное число элементов в множество.
	void add(set&); //Добавляет подмножество.

	//Перегрузки операторов:
	friend std::ostream& operator << (std::ostream&, const set<T>&); //Перегрузка оператора вывода множества на экран.
	friend std::istream& operator >> (std::istream&, set<T>&); //Перегрузка оператора ввода в множество нового элемента с клавиатуры.
	set<T>& operator=(const set<T>&); //Перегрузка оператора присвоения.
	set<T>& operator+(const set<T>&); //Перегрузка оператора объединения.
	set<T>& operator*(const set<T>&); //Перегрузка оператора пересечения.
	set<T>& operator-(const set<T>&); //Перегрузка оператора разности.
	bool operator==(set<T>&); //Перегрузка оператора равенства.
	set<T>& operator+(T); //Перегрузка оператора добавления элемента в множество (альтернатива add).

	// Перегрузка оператора вывода множества на экран.
	friend std::ostream& operator<<(std::ostream& out, const set<T>& s) {
		if (s.first == nullptr) return out << "Empty set." << std::endl;
		set_unit<T> *current = s.first;
		out << '{';
		while (current->next != nullptr) {
			out << current->content << ", ";
			current = current->next;
		}
		out << current->content << '}' << std::endl;
		return out;
	}
	//Перегрузка оператора ввода одного элемента с клавиатуры.
	friend std::istream& operator>>(std::istream& in, set<T>& s) {
		T temp;
		in >> temp;
		s.add(temp);
		return in;
	} 
};

template <typename T>
set<T>::set() {
	Size = 0;
	first = nullptr;
}

template<typename T>
set<T>::set(set<T>& s) {
	*this = s;
}

template <typename T>
set<T>::~set() {
	clear();
}

template<typename T>
void set<T>::removeFirst() {
	if (this->Size) {
		set_unit<T>* temp = this->first;
		this->first = first->next;
		delete temp;
		Size--;
	}
}

template<typename T>
void set<T>::remove(set<T>& s) {
	if (s.first != nullptr) {
		set_unit<T>* current = s.first;
		while (current->next != nullptr) {
			this->remove(current->content);
			current = current->next;
		}
		this->remove(current->content);
	}
}

template<typename T>
void set<T>::clear() {
	while (Size)
		removeFirst();
	first = nullptr;
}

template<typename T>
bool set<T>::isExists(T element) {
	if (first == nullptr)
		return false;
	else {
		set_unit<T>* current = this->first;
		while (current->next != nullptr) {
			if (current->content == element)
				return true;
			else
				current = current->next;
		}
		if (current->content == element)
			return true;
	}
	return false;
}

template<typename T>
void set<T>::add(set& s) {
	if (s.first != nullptr) {
		set_unit<T>* current = s.first;
		while (current->next != nullptr) {
			this->add(current->content);
			current = current->next;
		}
		this->add(current->content);
	}
}

template<typename T>
template<typename... Args>
set<T>::set(Args... args) {
	this->add(args...);
}

template<typename T>
template<typename... Args>
void set<T>::remove(T first, Args... args) {
	this->remove(first);
	this->remove(args...);
}

template<typename T>
template<typename... Args>
void set<T>::add(T content) {
	//Проверяем не содержит ли уже множество этот элемент.
	//Если не содержит, добавляем в конец связного списка.
	//Иначе - заканчиваем работу функции, ничего не добавляем.
	if (!this->isExists(content)) {
		//Проверяем не пусто ли множество.
		//Если пусто - создаем первый элемент.
		//Иначе - начинаем перебирать в цикле все элементы множества.
		if (first == nullptr)
			first = new set_unit<T>(content);
		else {
			//Временная переменная-указатель, которая будет указывать на следующий элемент в цикле.
			set_unit<T>* current = this->first;
			//Условие выхода из цикла - достижение последнего элемента, указывающего на NULL.
			while (current->next != nullptr) {
				current = current->next;
			}
			current->next = new set_unit<T>(content);
		}
		Size++;
	}
}

template<typename T>
template<typename... Args>
void set<T>::add(T first, Args... args) {
	add(first);
	add(args...);
}

template<typename T>
void set<T>::remove(T element) {
	if(this->isExists(element)) {
		if (this->first->content == element) {
			this->removeFirst();
		}
		else {
			set_unit<T>* current = this->first;
			while (current->next != nullptr) {
				if (current->next->content == element) {
					set_unit<T>* temp = current->next;
					current->next = current->next->next;
					delete temp;
					break;
				}
				current = current->next;
			}
			Size--;
		}
	}
}

template<typename T>
set<T>& set<T>::operator=(const set<T>& s) {
	clear();
	if (s.first == nullptr) return *this;
	set_unit<T>* current = s.first;
	while (current->next != nullptr) {
		add(current->content);
		current = current->next;
	}
	add(current->content);
	return *this;
}

template<typename T>
set<T>& set<T>::operator+(const set<T>& s) {
	set<T> *result = new set<T>;
	*result = *this;
	if (s.first == nullptr) return *result;
	set_unit<T>* current = s.first;
	while (current->next != nullptr) {
		result->add(current->content);
		current = current->next;
	}
	result->add(current->content);
	return *result;
}

template<typename T>
set<T>& set<T>::operator*(const set<T>& s) {
	set<T>* result = new set<T>;
	if (((*this).first == nullptr) or (s.first == nullptr)) return *result;
	set_unit<T>* current = s.first;
	while (current->next != nullptr) {
		if ((*this).isExists(current->content))
			result->add(current->content);
		current = current->next;
	}
	if ((*this).isExists(current->content))
		result->add(current->content);
	return *result;
}

template<typename T>
set<T>& set<T>::operator-(const set<T>& s) {
	set<T>* result = new set<T>;
	if ((*this).first == nullptr) return *result;
	*result = *this;
	set_unit<T>* current = s.first;
	while (current->next != nullptr) {
		result->remove(current->content);
		current = current->next;
	}
	result->remove(current->content);
	return *result;
}

template<typename T>
bool set<T>::operator==(set<T> & s) {
    if(this->getSize() != s.getSize()) return false;
    if((this->getSize() == s.getSize()) and (this->getSize() == 0))
        return true;
    bool equal = true;
    set_unit<T>* current = this->first;
    while (current->next != nullptr) {
        if(!(s.isExists(current->content))) equal = false;
        current = current->next;
    }
    if(!(s.isExists(current->content))) equal = false;
    return equal;
}

template<typename T>
set<T>& set<T>::operator+(T content) {
	set<T>* result = new set<T>(*this);
	result->add(content);
	return *result;
}

